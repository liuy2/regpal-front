var vm = new Vue({
    el: "#search_app",
    data: {
        hitsPerPage: 10,
        fedOrIndig: null,
        activity: [],
        naics: '',
        searchQuery: '',
        client: null,
        naicsIndex: null,
        searchHelper: null,
        searchResults: null,
        pagination: 0,
        totalRegs: 0,
        totalPages: 0,
        naicsString: '',
        currentStep: 1,
        briefcase: [],
        searchTab: 1, //1 signifies long search. 2 signifies kw search
    },
    watch: {
        //watch for variable changes
        searchQuery: function (val, oldVal) {
            this.search();
        },
        fedOrIndig: function(val, oldVal){
            this.search();
        },
        naics: {
            handler: function (val, oldVal) {
                this.search();
            }
        },
        naicsString: {
            handler: function (val, oldVal) {
                if (val == '' || val == null) {
                    this.naics = '';
                }
            }
        },
        activity: {
            handler: function(val, oldVal){
                this.search();
            }
        },
        deep: true,
    },
    mounted: function () {

        this.client = algoliasearch('IQKSBFPSWC', 'd3f4cedbe112e08a651ba3874b026253');

        this.naicsIndex = this.client.initIndex('cbn_naics_lookup_en');

        this.searchHelper = algoliasearchHelper(this.client, 'EC_Regpal_Demo', {
            hitsPerPage: this.hitsPerPage,
            facets: ['land_type'],
            disjunctiveFacets: ['regulation_naics', 'activity']
        });

        //the naics lookup for final step
        autocomplete('#naicsSearchFinal', { hint: true, autoselect: true }, {
            source: autocomplete.sources.hits(this.naicsIndex, { hitsPerPage: 5 }),
            displayKey: function (suggestion) {
                return suggestion.title;
            },
            templates: {
                suggestion: function (suggestion) {
                    return '<div>' + suggestion._highlightResult.title.value + '</div>';
                }
            }
        }).on('autocomplete:selected', function (event, suggestion, dataset) {
	        vm.naicsString = suggestion.title;
            var selectedNaics = suggestion.naics_code;
            vm.naics = selectedNaics.toString().substring(0, 3);
            vm.search();
        });

        //this is naics lookup for step 1
        autocomplete('#naicsSearchstep1', { hint: true, autoselect: true }, {
            source: autocomplete.sources.hits(this.naicsIndex, { hitsPerPage: 5 }),
            displayKey: function (suggestion) {
                return suggestion.title;
            },
            templates: {

                suggestion: function (suggestion) {
                    return '<div>' + suggestion._highlightResult.title.value + '</div>';
                }
            }
        }).on('autocomplete:selected', function (event, suggestion, dataset) {
            vm.naicsString = suggestion.title;
            var selectedNaics = suggestion.naics_code;
            vm.naics = selectedNaics.toString().substring(0, 3);
            vm.search();
        });

        //cant use this as the calling method originates outside of vm
        this.searchHelper.on('result', function (content) {
            vm.searchResults = content.hits;
            vm.pagination = content.page;
            vm.totalRegs = content.nbHits;
            vm.totalPages = content.nbPages;
        });

        //see if there is already hash
        if(location.hash.length != 0){
            if(location.hash[1] > 0 && location.hash[1] < 5){
                this.currentStep = location.hash[1];
            }
        }

        //back / forward button functionality
        //use hash change detection to change currentStep
        window.onhashchange = (function(){
            if(location.hash.length == 0){
                vm.currentStep = 1;
            } else if (location.hash.length == 2){
                vm.currentStep = location.hash[1];
            }
        });

        this.search();
    },
    methods: {
        search: function () {
            this.searchHelper.clearRefinements();
            //add refinements

            //naics
            this.searchHelper.addDisjunctiveFacetRefinement("regulation_naics", this.naics);

            //activities
            for(var i = 0; i < this.activity.length; i++){
                this.searchHelper.addDisjunctiveFacetRefinement('activity', parseInt(this.activity[i]));
            }

            //landtype
            if(this.fedOrIndig == "2"){
                //exclude federal or indigenous land
                this.searchHelper.addFacetExclusion('land_type', '1');
                this.searchHelper.addFacetExclusion('land_type', '2');
            }

            //keywords
            this.searchHelper.setQuery(this.searchQuery);
            this.searchHelper.search();

            //dynamically add the success alert if it does not exist yet
            if(vm.currentStep == 4 && $("#success_alert").length == 0){
                var success_alert_element = '<div style="width: 100%; position: fixed; top: 0; left: 0; z-index: 10000;" class="alert alert-success" id="success-alert"><strong>Your results have been updated based on your search query.</strong></div>';
                $('main').prepend(success_alert_element);
            }

            //trigger the success message if in results page
            if(vm.currentStep == 4){
                $("#success-alert").alert();
                //shows up for 1 second
                $("#success-alert").fadeTo(1500, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
            }
        },
        changePage: function (n) {
            this.pagination = n;
            this.searchHelper.setPage(n);
            this.searchHelper.search();
        },
        //given an array like [1,3,5], return an array of corresponding activities
        getActivity: function (array) {
            var values = [null, 'Buying', 'Disposing', 'Exporting', 'Importing', 'Manufacturing', 'Possessing', 'Selling', 'Storing', 'Transforming', 'Transporting', 'Using'];

            var output = ''

            array.forEach(function (i) {
                if(i != 0){
                    output += values[i] + ', ';
                }   
            })

            if(output.length > 0){
                output = output.substring(0, output.length - 2);
            }

            return [output];
        },
        //return corresponding array of landtypes
        getLandType: function (array) {
            var values = [null, 'Federal', 'Aboriginal', 'Critical Habitat'];
            var output = '';
            array.forEach(function (i) {
                if(i != 0){
                    output += values[i] + ', ';
                }
            })

            if(output.length > 0){
                output = output.substring(0, output.length - 2);
            }

            return [output];
        },
        addToBriefcase: function (item) {
            vm.briefcase.push(item);
        },
        removeFromBriefcase: function (item) {
            //use the jQuery grep to remove the said item
            vm.briefcase = $.grep(vm.briefcase, function (obj, id) {
                return obj.objectID === item.objectID;
            }, true);
        },
        isInBriefcase: function (item) {
            if (($.grep(vm.briefcase, function (obj, id) {
                return obj.objectID === item.objectID;
            })).length == 0) {
                return false;
            } else {
                return true;
            }
        },
        unfilterString: function(str) {
            return str;
        }
    }
});
